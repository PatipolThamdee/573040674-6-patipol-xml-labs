var jsonfile = require('jsonfile')

var file = 'data.json'
var obj = {
  "name": "CM",
  "courses": [
    "198330",
    "198371"
  ],
  "places": {
    "residence": "Khon Kaen",
    "visits": [
      "Songkla",
      "Bangkok"
    ]
  }
}


jsonfile.writeFile(file, obj, function (err) {
  console.error(err)
})


var file = 'data.json';
jsonfile.readFile(file, function(err, obj) {
  console.log("=== Output part 1 : The whole object from reading data.json === ");
  console.dir(obj);
  console.log("=== Output part 2 : The values of the second course and the residence === ");
  console.log("Studying "+obj.courses[1] + " living in "+obj.places.residence);

})
