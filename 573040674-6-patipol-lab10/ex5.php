<?php
header('Content-Type: application/xml');
$writer = new XMLWriter();

$writer->openMemory();

$writer->startDocument('1.0');

$writer->startElement('nations');
$writer->startElement('nation');

$writer->writeElement('name', 'Thailand');

$writer->writeElement('location', 'Southeast Asia');
$writer->endElement();

$writer->startElement('nation');
$writer->writeElement('name', 'USA');

$writer->writeElement('location', 'North America');
$writer->endElement();
$writer->endElement();
$writer->endDocument();



echo ($writer->outputMemory(true));
?>
