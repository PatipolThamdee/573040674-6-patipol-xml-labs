package lab11;
import java.io.*;
import java.net.URL;
import javax.json.*;
import javax.json.stream.*;

public class WriteNation {
    
    public static void main(String[] args) {
        FileWriter writer = null;
        try{
            writer = new FileWriter("nation.json");
        } catch(Exception ex){
            ex.printStackTrace();
        }
        JsonGenerator generator = Json.createGenerator(writer);
        generator.writeStartObject();
        generator.writeStartArray("nations");
        generator.writeStartObject();
        generator.write("name","Thailand");
        generator.write("location","South East Asia");
        generator.writeEnd();
        generator.writeStartObject();
        generator.write("name","USA");
        generator.write("location","North America");
        generator.writeEnd();
        generator.writeEnd();
        generator.writeEnd();
        generator.close();
    }
    
}
