import java.io.InputStream;
import java.net.URL;
import javax.json.*;
/**
 *
 * @author NName
 */
public class CallNation {
    public static void main(String[] args) {
        try{
        URL url = new URL("https://fb.kku.ac.th/krunapon/json/nations.json");
                InputStream is = url.openStream();
                JsonReader rdr = Json.createReader(is);
                JsonObject obj = rdr.readObject();
                JsonArray arr = obj.getJsonArray("nations");
                
                for (JsonObject nation : arr.getValuesAs(JsonObject.class)){
                    System.out.println(nation.getString("name") + " is in " +nation.getString("location"));
                }
                
        } catch(Exception ex){
            ex.printStackTrace(System.err);
        }
    }
            
}