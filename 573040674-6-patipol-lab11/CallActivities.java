package lab11;

import java.net.URL;
import javax.xml.stream.*;
import java.net.URL;
import java.io.*;
import java.util.Properties;
import java.io.*;
import java.net.MalformedURLException;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.InputStream;
import java.net.URL;
import javax.json.*;
import javax.json.stream.JsonGenerator;

public class CallActivities {

    public static void main(String[] args) {
        FileWriter writer = null;
        try {
            URL url = new URL("https://www.kku.ac.th/ikku/api/activities/services/topActivity.php");
            InputStream is = url.openStream();
            JsonReader rdr = Json.createReader(is);
            JsonObject obj = rdr.readObject();
            JsonArray arr = obj.getJsonArray("activities");
            writer = new FileWriter("src\\activities.json");

            JsonGenerator generator = Json.createGenerator(writer);
            generator.writeStartObject();
            generator.writeStartArray("activities");

            for (JsonObject nation : arr.getValuesAs(JsonObject.class)) {
                generator.writeStartObject();
                generator.write("title", nation.getString("title"));
                generator.write("url", nation.getString("url"));
                generator.writeEnd();

            }

            generator.writeEnd();
            generator.writeEnd();
            generator.close();

        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

}
