package lab11;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class CallGoogleMapDirectionXML {

    public static void main(String[] args) throws MalformedURLException, IOException {
       

        try {
            URL u = new URL("https://maps.googleapis.com/maps/api/directions/xml?origin=Khon%20Kaen&destination=Bangkok&key=AIzaSyCSzxPTtm9fj94EviSV7Ei3TL8TRqIxRng");
            InputStream in = u.openStream();
            XMLInputFactory factory
                    = XMLInputFactory.newInstance();
            XMLStreamReader parser
                    = factory.createXMLStreamReader(in);
            int inHeader = 0;
            String name = null;
            String location = null;
            int i=1;
            for (int event = parser.next();
                    event != XMLStreamConstants.END_DOCUMENT;
                    event = parser.next()) {
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        {
                            if(parser.getLocalName().equals("html_instructions")){
                               System.out.println(i+"."+ parser.getElementText());
                                i++;
                            }
                        }
                        break;
                   
                } // end switch
            } // end for
            parser.close();
            System.out.println("Done processing");
        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } 
    } // end main

}
