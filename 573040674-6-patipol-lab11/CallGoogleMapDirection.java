
package lab11;

import java.io.InputStream;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;


public class CallGoogleMapDirection {
    
        public static void main(String[] args) {
        try{
        URL url = new URL("https://maps.googleapis.com/maps/api/directions/json?origin=Khon%20Kaen&destination=Bangkok&key=AIzaSyCSzxPTtm9fj94EviSV7Ei3TL8TRqIxRng");
                InputStream is = url.openStream();
                JsonReader rdr = Json.createReader(is);
                JsonObject obj = rdr.readObject();
                
                JsonArray arr2 = obj.getJsonArray("routes");
                JsonObject obj2 = arr2.getJsonObject(0);
                JsonArray arr4 = obj2.getJsonArray("legs");
                JsonObject obj4 = arr4.getJsonObject(0);
                
                JsonArray arr3 = obj4.getJsonArray("steps");               
                int i = 1;
                for (JsonObject nation : arr3.getValuesAs(JsonObject.class)){
                    System.out.println(i+ "." + nation.getString("html_instructions"));
                    i++;
                }
                
        } catch(Exception ex){
            ex.printStackTrace(System.err);
        }
    }
    
}
