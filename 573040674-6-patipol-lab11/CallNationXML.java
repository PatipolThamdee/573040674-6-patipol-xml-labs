package lab11;

import javax.xml.stream.*;
import java.net.URL;
import java.io.*;
import java.util.Properties;
import java.io.*;
import java.net.MalformedURLException;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class CallNationXML {

    public static void main(String[] args) throws MalformedURLException, IOException {
       

        try {
            URL u = new URL("https://fb.kku.ac.th/krunapon/xml/nations.xml");
            InputStream in = u.openStream();
            XMLInputFactory factory
                    = XMLInputFactory.newInstance();
            XMLStreamReader parser
                    = factory.createXMLStreamReader(in);
            int inHeader = 0;
            String name = null;
            String location = null;
            for (int event = parser.next();
                    event != XMLStreamConstants.END_DOCUMENT;
                    event = parser.next()) {
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        {
                            if(parser.getLocalName().equals("name"))
                            name = parser.getElementText();
                            else if(parser.getLocalName().equals("location"))
                            location = parser.getElementText();
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        {
                            if(parser.getLocalName().equals("nation"))
                                System.out.println(name+" is in "+location);
                         

                        }
                        break;
                   
                } // end switch
            } // end for
            parser.close();
            System.out.println("Done processing");
        } catch (XMLStreamException ex) {
            System.out.println(ex);
        } 
    } // end main


}
