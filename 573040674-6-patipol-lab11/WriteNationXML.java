package lab11;

import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class WriteNationXML {

    public static void main(String[] args) {
        try {
            String fileName = "nation.xml";
            XMLOutputFactory xof
                    = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw
                    = xof.createXMLStreamWriter(new FileWriter(fileName));
            xtw.writeStartDocument();
            xtw.writeStartElement("nations");
            xtw.writeStartElement("nation");
            xtw.writeStartElement("name");
            xtw.writeCharacters("Thailand");
            xtw.writeEndElement(); // end name element
            xtw.writeStartElement("location");
            xtw.writeCharacters("Southeast Asia");
            xtw.writeEndElement(); // end location element
            xtw.writeEndElement(); // end nation element
            
            xtw.writeStartElement("nation");
            xtw.writeStartElement("name");
            xtw.writeCharacters("USA");
            xtw.writeEndElement(); // end name element

            xtw.writeStartElement("location");
            xtw.writeCharacters("North America");
            xtw.writeEndElement(); // end location element
            xtw.writeEndElement(); // end nation element
            
            
            xtw.writeEndElement();
            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();

        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

}
