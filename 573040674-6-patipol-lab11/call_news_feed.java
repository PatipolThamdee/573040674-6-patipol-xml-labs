package lab11;

import java.net.URL;
import javax.xml.stream.*;
import java.net.URL;
import java.io.*;
import java.util.Properties;
import java.io.*;
import java.net.MalformedURLException;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import java.io.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class call_news_feed {

    public static void main(String[] args) {
        try {
            URL u = new URL("http://www.bangkokbiznews.com/rss/feed/technology.xml");
            InputStream in = u.openStream();
            XMLInputFactory factory
                    = XMLInputFactory.newInstance();
            XMLStreamReader parser
                    = factory.createXMLStreamReader(in);
            String title = null;
            String url = null;

            String fileName = "news.xml";
            XMLOutputFactory xof
                    = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw
                    = xof.createXMLStreamWriter(new FileWriter(fileName));
            xtw.writeStartDocument();
            xtw.writeStartElement("news");
            xtw.writeStartElement("channel");
            for (int event = parser.next();
                    event != XMLStreamConstants.END_DOCUMENT;
                    event = parser.next()) {
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT: {
                        if (parser.getLocalName().equals("title")) {
                            title = parser.getElementText();
                        } else if (parser.getLocalName().equals("link")) {
                            url = parser.getElementText();
                        }
                    }
                    break;
                    case XMLStreamConstants.END_ELEMENT: {
                        if (parser.getLocalName().equals("item")) {
                            xtw.writeStartElement("item");
                            xtw.writeStartElement("title");  
                            xtw.writeCharacters(title);
                            xtw.writeEndElement(); // end name element
                            xtw.writeStartElement("url");
                            xtw.writeCharacters(url);
                            xtw.writeEndElement(); // end location element
                            xtw.writeEndElement(); // end nation element
                        }

                    }
                    break;

                }

               
               
            }
             xtw.writeEndElement();
                xtw.writeEndElement();
                xtw.writeEndDocument();
                xtw.flush();
                xtw.close();
        } catch (Exception ex) {
            ex.printStackTrace(System.err);
        }
    }

}
